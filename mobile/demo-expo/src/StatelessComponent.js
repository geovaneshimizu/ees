import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class TextView extends Component {
    render() {
        return (
            <View>
                <Text style={{ padding: 10, fontSize: 16 }}>{this.props.text}</Text>
            </View>
        );
    };
}
