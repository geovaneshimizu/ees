import React, { Component } from 'react';
import { StyleSheet, View, Button, TextInput } from 'react-native';

export default class FormView extends Component {
    
    state = {
      inputValue: ''
    };
  
    render() {
      return (
        <View>
          <TextInput
            placeholder='Type here'
            multiline={true}
            maxLength={20}
            style={styles.font}
            onChangeText={(value) => this.setState({inputValue: value})}
          />
          <Button title="Send" onPress={() => this.props.onSend(this.state.inputValue)} />
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    font: {
      marginTop: 20,
      paddingTop: 10,
      paddingRight: 10,
      paddingLeft: 10,
      color: 'black'
    }
  });
  