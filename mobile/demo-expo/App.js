import React from 'react';
import { View, StatusBar } from 'react-native';
import FormView from './src/StatefulComponent';
import TextView from './src/StatelessComponent';

export default class App extends React.Component {

  state = {
    formValue: ''
  };

  changeText = (value) => {
    this.setState({
      formValue: value
    });
  };

  render() {
    return (
      <View>
        <StatusBar barStyle='light-content' />
        <FormView onSend={this.changeText} />
        <TextView text={this.state.formValue} />
      </View>
    );
  }
}
