var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "pass"
});

con.connect((err) => {
    if (err) throw err;
    console.log("Connected");
    con.query("CREATE DATABASE mydb", (err, result) => {
        if (err) throw err;
        console.log("DB created");
    });
});
