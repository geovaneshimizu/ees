const mysql = require('mysql');

let con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "pass",
    database: "mydb"
});

con.connect((err) => {
    if (err) throw err;
    console.log("Connected");
    let sql = "CREATE TABLE IF NOT EXISTS customers (name VARCHAR(255), address VARCHAR(255))" 
    con.query(sql, (err, result) => {
        if (err) throw err;
        console.log("Table created");
    });
});
