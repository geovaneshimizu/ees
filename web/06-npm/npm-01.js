const http = require('http');
const uc = require('upper-case');

http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(uc.upperCase("hello uc"));
    res.end();
}).listen(8080);
