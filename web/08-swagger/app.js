const express = require('express');
const morgan = require('morgan');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const app = express();

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

/**
 * @swagger
 * /customers:
 *   get:
 *     description: Use to request all customers
 *     responses:
 *       '200':
 *         description: A successfull response
 */
app.get("/customers", (req, res) => {
    res.status(200).send("customers");
});

/**
 * @swagger
 * /customer:
 *   put:
 *     description: Use to create a customer
 *     parameters:
 *       - name: customer
 *         in: query
 *         description: Name of our customer
 *         required: false
 *         schema:
 *           type: string
 *           format: string
 *     responses:
 *       '201':
 *         description: A successfull response
 */
app.put("/customer", (req, res) => {
    res.status(201).send("customer updated");
});

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "Customer API",
            description: "Customer API information",
            contact: {
                name: "dev team"
            },
            servers: ["http://localhost:8000"]
        }
    },
    apis: ["app.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

module.exports = app;
