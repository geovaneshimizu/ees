var fs = require("fs");

console.log("Creating file");
fs.writeFile('input3.txt', 'To be removed', function (err) {
    if (err) {
        return console.error(err);
    }
    console.log("Data written successfully!");

    console.log("Deleting created file");
    fs.unlink('input3.txt', function (err) {
        if (err) {
            return console.error(err);
        }
        console.log("File deleted successfully!");
    })
});
