var fs = require("fs");

console.log("Reading current directory");
fs.readdir('.', (err, files) => {
    if (err) {
        return console.error(err);
    }
    files.forEach(file => {
        console.log(file);
    })
});
