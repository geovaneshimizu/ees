var fs = require("fs");

console.log("Creating directory tmp");
fs.mkdir('tmp', err => {
    if (err) {
        return console.error(err);
    }
    console.log("Dir created successfully!");
});
