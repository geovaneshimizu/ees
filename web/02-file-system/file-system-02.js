var fs = require("fs");

console.log("Writing into existing file");
fs.writeFile('input2.txt', 'Simply Easy Learning', function (err) {
    if (err) {
        return console.error(err);
    }
    console.log("Data written successfully!");
    console.log("Lets read newly written data");

    fs.readFile('input2.txt', function (err, data) {
        if (err) {
            return console.error(err);
        }
        console.log("Asynchronous read: " + data.toString());
    });
});
