const http = require('http');
const dt = require('./myfirstmodule');

http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write("Current date time: " + dt.myDateTime());
    res.end();
}).listen(8080);
